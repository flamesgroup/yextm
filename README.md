# YExtM

Java library to connect by TCP to [YATE extmodule](http://docs.yate.ro/wiki/External_Module) which implements [external modules protocol](http://yate.null.ro/docs/extmodule.html).

## YATE configuration

Add to `extmodule.conf` next configuration section:

    [listener for java]
    type=tcp
    addr=127.0.0.1
    port=10001
    role=global
    
## Java example


```java
import com.flamesgroup.yextmodule.IYExtM;
import com.flamesgroup.yextmodule.IYExtMChannel;
import com.flamesgroup.yextmodule.IYExtMConnection;
import com.flamesgroup.yextmodule.IYExtMMessageHandler;
import com.flamesgroup.yextmodule.Message;
import com.flamesgroup.yextmodule.YExtM;
import com.flamesgroup.yextmodule.YExtMChannel;
import com.flamesgroup.yextmodule.YExtMConnection;

import java.net.InetSocketAddress;

public class YExtMTimerExample {

  public static void main(String[] args) throws Exception {
    IYExtMChannel channel = new YExtMChannel();
    IYExtMConnection connection = new YExtMConnection(channel);
    IYExtM yExtM = new YExtM(connection);

    yExtM.connect(new InetSocketAddress("127.0.0.1", 10001));

    IYExtMMessageHandler handlerTimer = new IYExtMMessageHandler() {
      @Override public int getPriority() {
        return 100;
      }

      @Override public String getName() {
        return "engine.timer";
      }

      @Override public String getFilterValue() {
        return null;
      }

      @Override public String getFilterName() {
        return null;
      }

      @Override public boolean handleReceived(Message message) {
        System.out.println("YATE time: " + message.getParams().get("time"));
        return false;
      }
    };

    String engineVersion = yExtM.getParameter("engine.version");
    System.out.println("YATE engine.version: " + engineVersion);

    yExtM.install(handlerTimer);

    Thread.sleep(5 * 1000);

    yExtM.uninstall(handlerTimer);

    yExtM.disconnect();
  }

}
```

Output:

    YATE engine.version: 5.4.2
    YATE time: 1345802136
    YATE time: 1345802137
    YATE time: 1345802138
    YATE time: 1345802139
    YATE time: 1345802140
