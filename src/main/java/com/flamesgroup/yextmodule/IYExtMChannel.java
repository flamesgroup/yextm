package com.flamesgroup.yextmodule;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.ByteChannel;

public interface IYExtMChannel extends ByteChannel {

  void connect(SocketAddress socketAddress) throws IOException;

  boolean isConnected();

}
