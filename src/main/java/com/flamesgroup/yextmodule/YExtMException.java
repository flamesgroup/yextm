package com.flamesgroup.yextmodule;

public class YExtMException extends Exception {

  private static final long serialVersionUID = 7692101188977485447L;

  public YExtMException() {
  }

  public YExtMException(String message) {
    super(message);
  }

  public YExtMException(Throwable throwable) {
    super(throwable);
  }

  public YExtMException(String message, Throwable cause) {
    super(message, cause);
  }

}
