package com.flamesgroup.yextmodule;

public class YExtMDecodeException extends YExtMException {

  private static final long serialVersionUID = 9080611647050575276L;

  public YExtMDecodeException() {
  }

  public YExtMDecodeException(String message) {
    super(message);
  }

  public YExtMDecodeException(Throwable throwable) {
    super(throwable);
  }

  public YExtMDecodeException(String message, Throwable cause) {
    super(message, cause);
  }

}
