package com.flamesgroup.yextmodule;

import java.io.IOException;
import java.net.SocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public final class YExtMChannel implements IYExtMChannel {

  private final SocketChannel channel;
  private SocketAddress local;
  private SocketAddress remote;

  public YExtMChannel() throws IOException {
    channel = SocketChannel.open();
    channel.setOption(StandardSocketOptions.TCP_NODELAY, true);
    channel.configureBlocking(true);
  }

  @Override
  public void connect(SocketAddress socketAddress) throws IOException {
    channel.connect(socketAddress);
    local = channel.getLocalAddress();
    remote = channel.getRemoteAddress();
  }

  @Override
  public boolean isConnected() {
    return channel.isConnected();
  }

  @Override
  public int read(ByteBuffer dst) throws IOException {
    return channel.read(dst);
  }

  @Override
  public int write(ByteBuffer src) throws IOException {
    return channel.write(src);
  }

  @Override
  public boolean isOpen() {
    return channel.isOpen();
  }

  @Override
  public void close() throws IOException {
    channel.close();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(64);
    sb.append(this.getClass().getName());
    sb.append('[');
    if (channel.isConnected()) {
      sb.append("connected");
      sb.append(" local=");
      sb.append(local);
      sb.append(" remote=");
      sb.append(remote);
    } else if (channel.isOpen()) {
      sb.append("unconnected");
    } else {
      sb.append("closed");
    }
    sb.append(']');
    return sb.toString();
  }

}
