package com.flamesgroup.yextmodule;

public interface IYExtMMessageHandler {

  int getPriority();

  String getName();

  String getFilterName();

  String getFilterValue();

  boolean handleReceived(Message message);

}
