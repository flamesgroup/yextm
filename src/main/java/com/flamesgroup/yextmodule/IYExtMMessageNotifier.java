package com.flamesgroup.yextmodule;

public interface IYExtMMessageNotifier {

  String getName();

  void handleDispatched(Message message, boolean handled);

}
