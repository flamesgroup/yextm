package com.flamesgroup.yextmodule;

import java.io.IOException;
import java.net.SocketAddress;

public interface IYExtM {

  void connect(SocketAddress socketAddress, IYExtMHandler handler) throws IOException;

  void disconnect() throws IOException;

  boolean isConnected();

  void install(IYExtMMessageHandler handler) throws YExtMException, IOException, InterruptedException;

  void uninstall(IYExtMMessageHandler handler) throws YExtMException, IOException, InterruptedException;

  void watch(IYExtMMessageNotifier notifier) throws YExtMException, IOException, InterruptedException;

  void unwatch(IYExtMMessageNotifier notifier) throws YExtMException, IOException, InterruptedException;

  String getParameter(String name) throws YExtMException, IOException, InterruptedException;

  void setParameter(String name, String value) throws YExtMException, IOException, InterruptedException;

  void output(String log) throws YExtMException, IOException;

  boolean dispatch(Message message) throws YExtMException, IOException, InterruptedException;

}
