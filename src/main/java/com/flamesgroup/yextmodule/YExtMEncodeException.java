package com.flamesgroup.yextmodule;

public class YExtMEncodeException extends YExtMException {

  private static final long serialVersionUID = -619140406025426607L;

  public YExtMEncodeException() {
  }

  public YExtMEncodeException(String message) {
    super(message);
  }

  public YExtMEncodeException(Throwable throwable) {
    super(throwable);
  }

  public YExtMEncodeException(String message, Throwable cause) {
    super(message, cause);
  }

}
