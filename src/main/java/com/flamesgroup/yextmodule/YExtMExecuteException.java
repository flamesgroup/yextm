package com.flamesgroup.yextmodule;

public class YExtMExecuteException extends YExtMException {

  private static final long serialVersionUID = -1030802676455458249L;

  public YExtMExecuteException() {
  }

  public YExtMExecuteException(String message) {
    super(message);
  }

  public YExtMExecuteException(Throwable throwable) {
    super(throwable);
  }

  public YExtMExecuteException(String message, Throwable cause) {
    super(message, cause);
  }

}
