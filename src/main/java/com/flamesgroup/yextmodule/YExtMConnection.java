package com.flamesgroup.yextmodule;

import com.flamesgroup.yextmodule.command.CommandA2E;
import com.flamesgroup.yextmodule.command.CommandArgumentsDecode;
import com.flamesgroup.yextmodule.command.CommandArgumentsEncode;
import com.flamesgroup.yextmodule.command.CommandE2A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class YExtMConnection implements IYExtMConnection {

  private final Logger logger = LoggerFactory.getLogger(YExtMConnection.class);

  private final IYExtMChannel channel;
  private IYExtMConnectionHandler handler;

  private Thread connectionProcessingThread;

  private final Lock sendCommandLock = new ReentrantLock();

  private final ByteBuffer writeByteBuffer = ByteBuffer.allocate(8192);
  private final CharBuffer writeCharBuffer = CharBuffer.allocate(8192);
  private final ByteBuffer readByteBuffer = ByteBuffer.allocate(8192);
  private final CharBuffer readCharBuffer = CharBuffer.allocate(8192);

  private final CharsetDecoder charsetDecoder = StandardCharsets.UTF_8.newDecoder();
  private final CharsetEncoder charsetEncoder = StandardCharsets.UTF_8.newEncoder();

  public YExtMConnection(IYExtMChannel channel) {
    if (channel == null) {
      throw new IllegalArgumentException("Channel parameter must not be null");
    }

    this.channel = channel;
  }

  @Override
  public void connect(SocketAddress socketAddress, IYExtMConnectionHandler handler) throws IOException {
    if (socketAddress == null) {
      throw new IllegalArgumentException("SocketAddress parameter must not be null");
    }
    if (handler == null) {
      throw new IllegalArgumentException("Connection handler parameter must not be null");
    }

    channel.connect(socketAddress);

    this.handler = handler;

    connectionProcessingThread = new Thread(new ConnectionProcessingTask(), "YExtMConnectionThread(" + channel + ")");
    connectionProcessingThread.setPriority(Thread.NORM_PRIORITY + 1);
    connectionProcessingThread.setDaemon(true);
    connectionProcessingThread.start();
  }

  @Override
  public void sendCommand(CommandA2E commandA2E) throws YExtMException, IOException {
    if (!channel.isConnected()) {
      throw new IllegalStateException("[" + this + "] is disconnected");
    }

    sendCommandLock.lock();
    try {
      writeCharBuffer.clear();
      writeCharBuffer.put("%%");
      writeCharBuffer.put(commandA2E.getCommandType());
      writeCharBuffer.put(':');
      commandA2E.encodeArgs(new CommandArgumentsEncode(writeCharBuffer));
      writeCharBuffer.put('\n');
      writeCharBuffer.flip();
      writeCharBuffer.mark();

      writeByteBuffer.clear();
      charsetEncoder.reset();
      CoderResult encodeCoderResult = charsetEncoder.encode(writeCharBuffer, writeByteBuffer, true);
      CoderResult encodeFlushCoderResult = charsetEncoder.flush(writeByteBuffer);
      writeByteBuffer.flip();

      if (encodeCoderResult.isError() || encodeFlushCoderResult.isError()) {
        writeCharBuffer.reset();
        handler.handleEncodeCharBufferError(writeCharBuffer, encodeCoderResult, encodeFlushCoderResult);
      }

      channel.write(writeByteBuffer);
    } finally {
      sendCommandLock.unlock();
    }
  }

  @Override
  public void disconnect() throws IOException {
    connectionProcessingThread.interrupt();
    try {
      connectionProcessingThread.join();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    channel.close();
  }

  @Override
  public boolean isConnected() {
    return channel.isConnected();
  }

  private static enum CommandParseState {
    P1, P2, TYPE, ARGS, END
  }

  private static enum ErrorInParseState {
    E1, r2, r3, o4, r5, space6, i7, n8, colon9, ORIGINAL, END
  }

  private class ConnectionProcessingTask implements Runnable {

    @Override
    public void run() {
      logger.debug("[{}] creating command processing pool for [{}]", this, channel);
      final ExecutorService receiveCommandProcessingExecutor = Executors.newCachedThreadPool(new ThreadFactory() {
        private final AtomicInteger newThreadCounter = new AtomicInteger();

        @Override
        public Thread newThread(final Runnable r) {
          Thread t = new Thread(r, "YExtMCommandThread" + newThreadCounter.incrementAndGet());
          t.setDaemon(true);
          return t;
        }
      });

      CharBuffer argsCharBuffer = CharBuffer.allocate(readCharBuffer.capacity());
      String commandType = null;

      CommandParseState state = CommandParseState.P1;
      ErrorInParseState errorInState = ErrorInParseState.E1;

      while (!Thread.interrupted()) {

        readByteBuffer.clear();
        try {
          if (channel.read(readByteBuffer) < 0) {
            logger.warn("[{}] has reached end-of-stream", channel);
            handler.handleDisconnect();
            break;
          }
        } catch (ClosedByInterruptException e) {
          Thread.currentThread().interrupt();
          continue;
        } catch (IOException e) {
          logger.warn("Problem read form [" + channel + "]", e);
          handler.handleException(e);
          break;
        }
        readByteBuffer.flip();
        readByteBuffer.mark();

        readCharBuffer.clear();
        charsetDecoder.reset();
        CoderResult decodeCoderResult = charsetDecoder.decode(readByteBuffer, readCharBuffer, true);
        CoderResult decodeFlushCoderResult = charsetDecoder.flush(readCharBuffer);
        readCharBuffer.flip();

        if (decodeCoderResult.isError() || decodeFlushCoderResult.isError()) {
          readByteBuffer.reset();
          handler.handleDecodeByteBufferError(readByteBuffer, decodeCoderResult, decodeFlushCoderResult);
        }

        while (readCharBuffer.hasRemaining()) {

          if (!argsCharBuffer.hasRemaining()) {
            logger.warn("Command part is to long (command parse state: {}; error in parse state: {}) - reset parse states and look for next Command", state, errorInState);
            state = CommandParseState.P1;
            errorInState = ErrorInParseState.E1;
            argsCharBuffer.clear();
            commandType = null;
          }

          if (state == CommandParseState.END) {
            state = CommandParseState.P1;
            argsCharBuffer.clear();
            commandType = null;
          }

          if (errorInState == ErrorInParseState.END) {
            logger.warn("[{}] - Error in:{}", YExtMConnection.this, argsCharBuffer);
            errorInState = ErrorInParseState.E1;
            argsCharBuffer.clear();
          }

          char ch = readCharBuffer.get();

          switch (errorInState) {
            case E1:
              errorInState = (ch == 'E') ? ErrorInParseState.r2 : ErrorInParseState.E1;
              break;
            case r2:
              errorInState = (ch == 'r') ? ErrorInParseState.r3 : ErrorInParseState.E1;
              break;
            case r3:
              errorInState = (ch == 'r') ? ErrorInParseState.o4 : ErrorInParseState.E1;
              break;
            case o4:
              errorInState = (ch == 'o') ? ErrorInParseState.r5 : ErrorInParseState.E1;
              break;
            case r5:
              errorInState = (ch == 'r') ? ErrorInParseState.space6 : ErrorInParseState.E1;
              break;
            case space6:
              errorInState = (ch == ' ') ? ErrorInParseState.i7 : ErrorInParseState.E1;
              break;
            case i7:
              errorInState = (ch == 'i') ? ErrorInParseState.n8 : ErrorInParseState.E1;
              break;
            case n8:
              errorInState = (ch == 'n') ? ErrorInParseState.colon9 : ErrorInParseState.E1;
              break;
            case colon9:
              errorInState = (ch == ':') ? ErrorInParseState.ORIGINAL : ErrorInParseState.E1;
              break;
            case ORIGINAL:
              if (ch == '\n') {
                errorInState = ErrorInParseState.END;
                argsCharBuffer.flip();
              } else {
                argsCharBuffer.put(ch);
              }
              break;
            default:
              throw new IllegalStateException();
          }

          if (errorInState != ErrorInParseState.ORIGINAL && errorInState != ErrorInParseState.END) {
            switch (state) {
              case P1:
                state = (ch == '%') ? CommandParseState.P2 : CommandParseState.P1;
                break;
              case P2:
                state = (ch == '%') ? CommandParseState.TYPE : CommandParseState.P1;
                break;
              case TYPE:
                if (ch == ':') {
                  state = CommandParseState.ARGS;
                  argsCharBuffer.flip();
                  commandType = argsCharBuffer.toString();
                  argsCharBuffer.clear();
                } else {
                  argsCharBuffer.put(ch);
                }
                break;
              case ARGS:
                if (ch == '\n') {
                  state = CommandParseState.END;
                } else {
                  argsCharBuffer.put(ch);
                }
                break;
              default:
                throw new IllegalStateException();
            }

            if (state == CommandParseState.END) {
              if (commandType == null) {
                throw new AssertionError("CommandE2A string command type must not be null");
              }

              CommandE2A commandE2A;
              try {
                commandE2A = CommandE2A.createByCommandType(commandType);
              } catch (YExtMDecodeException e) {
                logger.warn("Problem create CommandE2A subclass instance", e);
                handler.handleCreateCommandException(commandType, e);
                continue;
              }

              argsCharBuffer.flip();
              try {
                commandE2A.decodeArgs(new CommandArgumentsDecode(argsCharBuffer));
              } catch (YExtMDecodeException e) {
                logger.warn("Problem decode CommandE2A with string command type [" + commandType + "]", e);
                handler.handleDecodeCommandException(commandType, e);
                continue;
              } finally {
                argsCharBuffer.clear();
              }

              receiveCommandProcessingExecutor.execute(new HandleReceiveCommandTask(commandE2A));
            }
          }
        }
      }
      receiveCommandProcessingExecutor.shutdown();
    }
  }

  private class HandleReceiveCommandTask implements Runnable {

    private final CommandE2A commandE2A;

    public HandleReceiveCommandTask(CommandE2A commandE2A) {
      this.commandE2A = commandE2A;
    }

    @Override
    public void run() {
      try {
        handler.handleReceiveCommand(commandE2A);
      } catch (Exception e) {
        logger.warn("Shield against uncaught command receive handler exception", e);
      }
    }

  }

}
