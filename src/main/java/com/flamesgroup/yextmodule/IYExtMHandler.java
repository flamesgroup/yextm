package com.flamesgroup.yextmodule;

import java.io.IOException;

public interface IYExtMHandler {

  void handleDisconnect();

  void handleException(IOException e);

}
