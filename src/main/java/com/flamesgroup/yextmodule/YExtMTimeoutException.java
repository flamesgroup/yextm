package com.flamesgroup.yextmodule;

public class YExtMTimeoutException extends YExtMException {

  private static final long serialVersionUID = -7688106273264183199L;

  public YExtMTimeoutException() {
  }

  public YExtMTimeoutException(String message) {
    super(message);
  }

  public YExtMTimeoutException(Throwable throwable) {
    super(throwable);
  }

  public YExtMTimeoutException(String message, Throwable cause) {
    super(message, cause);
  }

}
