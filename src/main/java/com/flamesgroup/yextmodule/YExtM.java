package com.flamesgroup.yextmodule;

import com.flamesgroup.yextmodule.command.CommandE2A;
import com.flamesgroup.yextmodule.command.CommandTransaction;
import com.flamesgroup.yextmodule.command.InstallCommandA2E;
import com.flamesgroup.yextmodule.command.InstallCommandE2A;
import com.flamesgroup.yextmodule.command.MessageRequestCommandA2E;
import com.flamesgroup.yextmodule.command.MessageRequestCommandE2A;
import com.flamesgroup.yextmodule.command.MessageResponseCommandA2E;
import com.flamesgroup.yextmodule.command.MessageResponseCommandE2A;
import com.flamesgroup.yextmodule.command.OutputCommandA2E;
import com.flamesgroup.yextmodule.command.SetlocalCommandA2E;
import com.flamesgroup.yextmodule.command.SetlocalCommandE2A;
import com.flamesgroup.yextmodule.command.UninstallCommandA2E;
import com.flamesgroup.yextmodule.command.UninstallCommandE2A;
import com.flamesgroup.yextmodule.command.UnwatchCommandA2E;
import com.flamesgroup.yextmodule.command.UnwatchCommandE2A;
import com.flamesgroup.yextmodule.command.WatchCommandA2E;
import com.flamesgroup.yextmodule.command.WatchCommandE2A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class YExtM implements IYExtM {

  private final Logger logger = LoggerFactory.getLogger(YExtM.class);

  private final IYExtMConnection connection;
  private final IYExtMConnectionHandler connectionHandler;

  private final long timeout = 10000;

  private final Lock installCommandLock = new ReentrantLock();
  private final CommandTransaction<InstallCommandA2E, InstallCommandE2A> installCommandTransaction;
  private final Lock uninstallCommandLock = new ReentrantLock();
  private final CommandTransaction<UninstallCommandA2E, UninstallCommandE2A> uninstallCommandTransaction;
  private final Lock watchCommandLock = new ReentrantLock();
  private final CommandTransaction<WatchCommandA2E, WatchCommandE2A> watchCommandTransaction;
  private final Lock unwatchCommandLock = new ReentrantLock();
  private final CommandTransaction<UnwatchCommandA2E, UnwatchCommandE2A> unwatchCommandTransaction;
  private final Lock setlocalCommandLock = new ReentrantLock();
  private final CommandTransaction<SetlocalCommandA2E, SetlocalCommandE2A> setlocalCommandTransaction;

  private final Lock stateLock = new ReentrantLock();
  private IYExtMHandler handler;

  private final Map<String, IYExtMMessageHandler> messageHandlers = new ConcurrentHashMap<>();
  private final Map<String, IYExtMMessageNotifier> messageNotifiers = new ConcurrentHashMap<>();
  private final Map<String, CommandTransaction<MessageRequestCommandA2E, MessageResponseCommandE2A>> dispatchedMessages = new ConcurrentHashMap<>();

  public YExtM(IYExtMConnection connection) {
    if (connection == null) {
      throw new IllegalArgumentException("connection must not be null");
    }
    this.connection = connection;
    connectionHandler = new YExtMConnectionHandler();

    installCommandTransaction = new CommandTransaction<>(connection, timeout);
    uninstallCommandTransaction = new CommandTransaction<>(connection, timeout);
    watchCommandTransaction = new CommandTransaction<>(connection, timeout);
    unwatchCommandTransaction = new CommandTransaction<>(connection, timeout);
    setlocalCommandTransaction = new CommandTransaction<>(connection, timeout);
  }

  @Override
  public void connect(SocketAddress socketAddress, IYExtMHandler handler) throws IOException {
    Objects.requireNonNull(socketAddress, "socketAddress mustn't be null");
    Objects.requireNonNull(handler, "handler mustn't be null");

    stateLock.lock();
    try {
      this.handler = handler;
      connection.connect(socketAddress, connectionHandler);
    } finally {
      stateLock.unlock();
    }
  }

  @Override
  public void disconnect() throws IOException {
    stateLock.lock();
    try {
      connection.disconnect();

      handler = null;
      messageHandlers.clear();
      messageNotifiers.clear();
    } finally {
      stateLock.unlock();
    }

  }

  @Override
  public boolean isConnected() {
    return connection.isConnected();
  }

  @Override
  public void install(IYExtMMessageHandler handler) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(handler, "handler mustn't be null");

    InstallCommandA2E installCommandA2E = new InstallCommandA2E(handler.getPriority(), handler.getName(), handler.getFilterName(), handler.getFilterValue());
    InstallCommandE2A installCommandE2A;

    installCommandLock.lock();
    try {
      installCommandE2A = installCommandTransaction.execute(installCommandA2E);
    } finally {
      installCommandLock.unlock();
    }

    if (!(installCommandE2A.getName().equals(handler.getName()))) {
      throw new YExtMException("Mismatching [" + installCommandA2E + "] with [" + installCommandE2A + "]");
    }

    if (installCommandE2A.isSuccess()) {
      messageHandlers.put(handler.getName(), handler);
    } else {
      throw new YExtMExecuteException("Can't install [" + handler + "]");
    }
  }

  @Override
  public void uninstall(IYExtMMessageHandler handler) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(handler, "handler mustn't be null");

    UninstallCommandA2E uninstallCommandA2E = new UninstallCommandA2E(handler.getName());
    UninstallCommandE2A uninstallCommandE2A;

    uninstallCommandLock.lock();
    try {
      uninstallCommandE2A = uninstallCommandTransaction.execute(uninstallCommandA2E);
    } finally {
      uninstallCommandLock.unlock();
    }

    if (!(uninstallCommandE2A.getName().equals(handler.getName()))) {
      throw new YExtMException("Mismatching [" + uninstallCommandA2E + "] with [" + uninstallCommandE2A + "]");
    }

    if (uninstallCommandE2A.isSuccess()) {
      messageHandlers.remove(handler.getName());
    } else {
      throw new YExtMExecuteException("Can't uninstall [" + handler + "]");
    }
  }

  @Override
  public void watch(IYExtMMessageNotifier notifier) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(notifier, "notifier mustn't be null");

    WatchCommandA2E watchCommandA2E = new WatchCommandA2E(notifier.getName());
    WatchCommandE2A watchCommandE2A;

    watchCommandLock.lock();
    try {
      watchCommandE2A = watchCommandTransaction.execute(watchCommandA2E);
    } finally {
      watchCommandLock.unlock();
    }

    if (!(watchCommandE2A.getName().equals(notifier.getName()))) {
      throw new YExtMException("Mismatching [" + watchCommandA2E + "] with [" + watchCommandE2A + "]");
    }

    if (watchCommandE2A.isSuccess()) {
      messageNotifiers.put(notifier.getName(), notifier);
    } else {
      throw new YExtMExecuteException("Can't watch [" + notifier + "]");
    }
  }

  @Override
  public void unwatch(IYExtMMessageNotifier notifier) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(notifier, "notifier mustn't be null");

    UnwatchCommandA2E unwatchCommandA2E = new UnwatchCommandA2E(notifier.getName());
    UnwatchCommandE2A unwatchCommandE2A;

    unwatchCommandLock.lock();
    try {
      unwatchCommandE2A = unwatchCommandTransaction.execute(unwatchCommandA2E);
    } finally {
      unwatchCommandLock.unlock();
    }

    if (!(unwatchCommandE2A.getName().equals(notifier.getName()))) {
      throw new YExtMException("Mismatching [" + unwatchCommandA2E + "] with [" + unwatchCommandE2A + "]");
    }

    if (unwatchCommandE2A.isSuccess()) {
      messageNotifiers.remove(notifier.getName());
    } else {
      throw new YExtMExecuteException("Can't unwatch [" + notifier + "]");
    }
  }

  @Override
  public String getParameter(String name) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(name, "name mustn't be null");

    SetlocalCommandA2E setlocalCommandA2E = new SetlocalCommandA2E(name, "");
    SetlocalCommandE2A setlocalCommandE2A;

    setlocalCommandLock.lock();
    try {
      setlocalCommandE2A = setlocalCommandTransaction.execute(setlocalCommandA2E);
    } finally {
      setlocalCommandLock.unlock();
    }

    if (!(setlocalCommandE2A.getName().equals(name))) {
      throw new YExtMException("Mismatching [" + setlocalCommandA2E + "] with [" + setlocalCommandE2A + "]");
    }

    if (setlocalCommandE2A.isSuccess()) {
      return setlocalCommandE2A.getValue();
    } else {
      throw new YExtMExecuteException("Can't get parameter [" + name + "]");
    }
  }

  @Override
  public void setParameter(String name, String value) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(name, "name mustn't be null");
    Objects.requireNonNull(value, "value mustn't be null");

    SetlocalCommandA2E setlocalCommandA2E = new SetlocalCommandA2E(name, value);
    SetlocalCommandE2A setlocalCommandE2A;

    setlocalCommandLock.lock();
    try {
      setlocalCommandE2A = setlocalCommandTransaction.execute(setlocalCommandA2E);
    } finally {
      setlocalCommandLock.unlock();
    }

    if (!(setlocalCommandE2A.getName().equals(name))) {
      throw new YExtMException("Mismatching [" + setlocalCommandA2E + "] with [" + setlocalCommandE2A + "]");
    }

    if (!setlocalCommandE2A.isSuccess()) {
      throw new YExtMExecuteException("Can't set parameter [" + name + "]");
    }
  }

  @Override
  public void output(String log) throws YExtMException, IOException {
    Objects.requireNonNull(log, "log mustn't be null");

    connection.sendCommand(new OutputCommandA2E(log));
  }

  @Override
  public boolean dispatch(Message message) throws YExtMException, IOException, InterruptedException {
    Objects.requireNonNull(message, "message mustn't be null");

    CommandTransaction<MessageRequestCommandA2E, MessageResponseCommandE2A> commandTransaction = new CommandTransaction<>(connection, timeout);
    dispatchedMessages.put(message.getId(), commandTransaction);

    MessageRequestCommandA2E messageRequestCommandA2E = new MessageRequestCommandA2E(message.getId(), message.getTime(), message.getName(), message.getRetvalue(), message.getParams());

    MessageResponseCommandE2A messageResponseCommandE2A = commandTransaction.execute(messageRequestCommandA2E);
    if (!messageResponseCommandE2A.getName().isEmpty()) {
      message.setName(messageResponseCommandE2A.getName());
    }
    message.setRetvalue(messageResponseCommandE2A.getRetvalue());
    message.setParams(messageResponseCommandE2A.getParams());
    return messageResponseCommandE2A.isProcessed();
  }

  private void handleReceiveMessageRequestCommand(MessageRequestCommandE2A messageRequestCommandE2A) {
    IYExtMMessageHandler messageHandler = messageHandlers.get(messageRequestCommandE2A.getName());
    if (messageHandler == null) {
      logger.warn("Can't find message handler for [{}]", messageRequestCommandE2A);
    } else {
      Message message = new Message(messageRequestCommandE2A.getId(), messageRequestCommandE2A.getTime(), messageRequestCommandE2A.getName(), messageRequestCommandE2A.getRetvalue(),
          messageRequestCommandE2A.getParams());
      boolean processed = messageHandler.handleReceived(message);
      MessageResponseCommandA2E messageResponseCommandA2E =
          new MessageResponseCommandA2E(message.getId(), processed, message.getName().equals(messageRequestCommandE2A.getName()) ? "" : message.getName(), message.getRetvalue(), message.getParams());

      try {
        connection.sendCommand(messageResponseCommandA2E);
      } catch (YExtMException | IOException e) {
        logger.error("Can't send [{}] command", messageResponseCommandA2E, e);
      }
    }
  }

  private void handleReceiveMessageResponseCommand(MessageResponseCommandE2A messageResponseCommandE2A) {
    if (messageResponseCommandE2A.getId().isEmpty()) {
      IYExtMMessageNotifier messageNotifier = messageNotifiers.get(messageResponseCommandE2A.getName());
      if (messageNotifier == null) {
        logger.warn("Can't find message notifier for [{}]", messageResponseCommandE2A);
      } else {
        Message message = new Message(messageResponseCommandE2A.getId(), 0, messageResponseCommandE2A.getName(), messageResponseCommandE2A.getRetvalue(), messageResponseCommandE2A.getParams());
        messageNotifier.handleDispatched(message, messageResponseCommandE2A.isProcessed());
      }
    } else {
      CommandTransaction<MessageRequestCommandA2E, MessageResponseCommandE2A> commandTransaction = dispatchedMessages.remove(messageResponseCommandE2A.getId());
      if (commandTransaction != null) {
        commandTransaction.handleResponse(messageResponseCommandE2A);
      } else {
        logger.warn("Can't find proper command transaction for [{}]", messageResponseCommandE2A);
      }
    }
  }

  private class YExtMConnectionHandler implements IYExtMConnectionHandler {

    @Override
    public void handleReceiveCommand(CommandE2A commandE2A) {
      if (commandE2A instanceof MessageRequestCommandE2A) {
        handleReceiveMessageRequestCommand((MessageRequestCommandE2A) commandE2A);
      } else if (commandE2A instanceof MessageResponseCommandE2A) {
        handleReceiveMessageResponseCommand((MessageResponseCommandE2A) commandE2A);
      } else if (commandE2A instanceof InstallCommandE2A) {
        installCommandTransaction.handleResponse((InstallCommandE2A) commandE2A);
      } else if (commandE2A instanceof UninstallCommandE2A) {
        uninstallCommandTransaction.handleResponse((UninstallCommandE2A) commandE2A);
      } else if (commandE2A instanceof WatchCommandE2A) {
        watchCommandTransaction.handleResponse((WatchCommandE2A) commandE2A);
      } else if (commandE2A instanceof UnwatchCommandE2A) {
        unwatchCommandTransaction.handleResponse((UnwatchCommandE2A) commandE2A);
      } else if (commandE2A instanceof SetlocalCommandE2A) {
        setlocalCommandTransaction.handleResponse((SetlocalCommandE2A) commandE2A);
      } else {
        logger.error("Unknown CommandE2A [{}]", commandE2A);
      }
    }

    @Override
    public void handleDisconnect() {
      handler.handleDisconnect();
    }

    @Override
    public void handleException(final IOException e) {
      handler.handleException(e);
    }

  }

}
