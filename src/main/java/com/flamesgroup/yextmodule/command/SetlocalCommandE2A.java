package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

public class SetlocalCommandE2A extends CommandE2A {

  private String name;
  private String value;
  private boolean success;

  public SetlocalCommandE2A() {
    super();
  }

  @Override
  public String getCommandType() {
    return "<setlocal";
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }

  public boolean isSuccess() {
    return success;
  }

  @Override
  public void decodeArgs(CommandArgumentsDecode in) throws YExtMDecodeException {
    name = in.getString();
    value = in.getString();
    success = in.getBoolean();
  }

}
