package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMException;

public class CommandTransactionNowExecutingException extends YExtMException {

  private static final long serialVersionUID = 447734665786124959L;

  public CommandTransactionNowExecutingException() {
  }

  public CommandTransactionNowExecutingException(String message) {
    super(message);
  }

  public CommandTransactionNowExecutingException(Throwable throwable) {
    super(throwable);
  }

  public CommandTransactionNowExecutingException(String message, Throwable cause) {
    super(message, cause);
  }

}
