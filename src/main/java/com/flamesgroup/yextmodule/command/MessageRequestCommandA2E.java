package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

import java.util.Map;
import java.util.Map.Entry;

public class MessageRequestCommandA2E extends CommandA2E {

  private final String id;
  private final long time;
  private final String name;
  private final String retvalue;
  private final Map<String, String> params;

  public MessageRequestCommandA2E(String id, long time, String name, String retvalue, Map<String, String> params) {
    super();
    this.id = id;
    this.time = time;
    this.name = name;
    this.retvalue = retvalue;
    this.params = params;
  }

  @Override
  public String getCommandType() {
    return ">message";
  }

  public String getId() {
    return id;
  }

  public long getTime() {
    return time;
  }

  public String getName() {
    return name;
  }

  public String getRetvalue() {
    return retvalue;
  }

  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    out.putString(id);
    out.putLong(time);
    out.putString(name);
    out.putString(retvalue);
    for (Entry<String, String> entry : params.entrySet()) {
      out.putMapEntry(entry);
    }
  }

}
