package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

public class InstallCommandA2E extends CommandA2E {

  private final static long priorityDefault = 100;
  private final long priority;
  private final String name;
  private final String filterName;
  private final String filterValue;

  public InstallCommandA2E(String name) {
    this(priorityDefault, name, null, null);
  }

  public InstallCommandA2E(long priority, String name) {
    this(priority, name, null, null);
  }

  public InstallCommandA2E(String name, String filterName, String filterValue) {
    this(priorityDefault, name, filterName, filterValue);
  }

  public InstallCommandA2E(long priority, String name, String filterName, String filterValue) {
    super();
    if (priority < 0) {
      throw new IllegalArgumentException("Priority parameter must not be < 0");
    }
    if (name == null) {
      throw new IllegalArgumentException("Name parameter must not be null");
    }
    this.priority = priority;
    this.name = name;
    this.filterName = filterName;
    this.filterValue = filterValue;
  }

  @Override
  public String getCommandType() {
    return ">install";
  }

  public long getPriority() {
    return priority;
  }

  public String getName() {
    return name;
  }

  public String getFilterName() {
    return filterName;
  }

  public String getFilterValue() {
    return filterValue;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    if (priority == priorityDefault) {
      out.putString("");
    } else {
      out.putLong(priority);
    }
    out.putString(name);
    if (filterName != null) {
      out.putString(filterName);
      if (filterValue != null) {
        out.putString(filterValue);
      }
    }
  }

}
