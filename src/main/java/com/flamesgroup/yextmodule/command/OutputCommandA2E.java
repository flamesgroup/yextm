package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

public class OutputCommandA2E extends CommandA2E {

  private final String arbitraryUnescapedString;

  public OutputCommandA2E(String arbitraryUnescapedString) {
    super();
    this.arbitraryUnescapedString = arbitraryUnescapedString;
  }

  @Override
  public String getCommandType() {
    return ">output";
  }

  public String getOutput() {
    return arbitraryUnescapedString;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    out.putString(arbitraryUnescapedString, false);
  }

}
