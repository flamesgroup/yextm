package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

public abstract class CommandA2E extends Command {

  public abstract void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException;

}
