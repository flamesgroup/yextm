package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

public class UninstallCommandE2A extends CommandE2A {

  private long priority;
  private String name;
  private boolean success;

  public UninstallCommandE2A() {
    super();
  }

  @Override
  public String getCommandType() {
    return "<uninstall";
  }

  public long getPriority() {
    return priority;
  }

  public String getName() {
    return name;
  }

  public boolean isSuccess() {
    return success;
  }

  @Override
  public void decodeArgs(CommandArgumentsDecode in) throws YExtMDecodeException {
    priority = in.getLong();
    name = in.getString();
    success = in.getBoolean();
  }

}
