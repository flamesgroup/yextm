package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

public class UninstallCommandA2E extends CommandA2E {

  private final String name;

  public UninstallCommandA2E(String name) {
    super();
    this.name = name;
  }

  @Override
  public String getCommandType() {
    return ">uninstall";
  }

  public String getName() {
    return name;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    out.putString(name);
  }

}
