package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MessageRequestCommandE2A extends CommandE2A {

  private String id;
  private long time;
  private String name;
  private String retvalue;
  private final Map<String, String> params = new HashMap<>();

  public MessageRequestCommandE2A() {
    super();
  }

  @Override
  public String getCommandType() {
    return ">message";
  }

  public String getId() {
    return id;
  }

  public long getTime() {
    return time;
  }

  public String getName() {
    return name;
  }

  public String getRetvalue() {
    return retvalue;
  }

  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public void decodeArgs(CommandArgumentsDecode in) throws YExtMDecodeException {
    id = in.getString();
    time = in.getLong();
    name = in.getString();
    retvalue = in.getString();
    while (in.hasMoreDataToDecode()) {
      Entry<String, String> entry = in.getMapEntry();
      params.put(entry.getKey(), entry.getValue());
    }
  }
}
