package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

public class WatchCommandE2A extends CommandE2A {

  private String name;
  private boolean success;

  public WatchCommandE2A() {
    super();
  }

  @Override
  public String getCommandType() {
    return "<watch";
  }

  public String getName() {
    return name;
  }

  public boolean isSuccess() {
    return success;
  }

  @Override
  public void decodeArgs(CommandArgumentsDecode in) throws YExtMDecodeException {
    name = in.getString();
    success = in.getBoolean();
  }

}
