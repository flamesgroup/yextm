package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

import java.nio.BufferUnderflowException;
import java.nio.CharBuffer;
import java.util.AbstractMap;
import java.util.Map;

public final class CommandArgumentsDecode {

  private final CharBuffer argumentsCharBuffer;

  public CommandArgumentsDecode(CharBuffer argumentsCharBuffer) {
    super();
    this.argumentsCharBuffer = argumentsCharBuffer;
  }

  public boolean hasMoreDataToDecode() {
    return argumentsCharBuffer.hasRemaining();
  }

  public boolean getBoolean() throws YExtMDecodeException {
    String value = getStringWithoutDecode();
    if (value.equals("true")) {
      return true;
    } else if (value.equals("false")) {
      return false;
    } else {
      throw new YExtMDecodeException("Can't decode boolean from string [" + value + "]");
    }
  }

  public long getLong() throws YExtMDecodeException {
    String value = getStringWithoutDecode();
    try {
      return Long.parseLong(value);
    } catch (NumberFormatException e) {
      throw new YExtMDecodeException("Can't decode long from string [" + value + "]", e);
    }
  }

  public String getString() throws YExtMDecodeException {
    return getString(true);
  }

  public String getString(boolean escaped) throws YExtMDecodeException {
    if (escaped) {
      return getStringWithDecode();
    } else {
      return getStringWithoutDecode();
    }
  }

  public Map.Entry<String, String> getMapEntry() throws YExtMDecodeException {
    String keyvalue = getStringWithDecode();
    int equalIndex = keyvalue.indexOf('=');
    if (equalIndex == -1) {
      throw new YExtMDecodeException("Can't decode MapEntry (equal character isn't exist) from string [" + keyvalue + "]");
    } else {
      String key = keyvalue.substring(0, equalIndex);
      String value = keyvalue.substring(equalIndex + 1);
      return new AbstractMap.SimpleEntry<>(key, value);
    }
  }

  private char getCharWithoutDecode() throws YExtMDecodeException {
    try {
      return argumentsCharBuffer.get();
    } catch (BufferUnderflowException e) {
      throw new YExtMDecodeException("Can't decode - arguments CharBuffer buffer is empty", e);
    }
  }

  private String getStringWithoutDecode() throws YExtMDecodeException {
    StringBuilder stringBuilder = new StringBuilder(64);
    do {
      char ch = getCharWithoutDecode();
      if (ch == ':') {
        break;
      }
      stringBuilder.append(ch);
    } while (argumentsCharBuffer.hasRemaining());
    return stringBuilder.toString();
  }

  private String getStringWithDecode() throws YExtMDecodeException {
    StringBuilder stringBuilder = new StringBuilder(64);
    do {
      char ch = getCharWithoutDecode();
      if (ch == ':') {
        break;
      } else if (ch == '%') {
        ch = getCharWithoutDecode();
        if ((ch > '@' && ch <= '_') || ch == 'z') {
          ch -= '@';
        } else if (ch != '%') {
          throw new YExtMDecodeException("Can't decode - incorrect %-escaped character [" + ch + "]");
        }
      }
      stringBuilder.append(ch);
    } while (argumentsCharBuffer.hasRemaining());
    return stringBuilder.toString();
  }

}
