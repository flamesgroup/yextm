package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class CommandE2A extends Command {

  private static Map<String, Class<? extends CommandE2A>> commandTypeToCommandE2AClassMap = new HashMap<>();

  public abstract void decodeArgs(CommandArgumentsDecode in) throws YExtMDecodeException;

  public static CommandE2A createByCommandType(String commandType) throws YExtMDecodeException {
    Class<? extends CommandE2A> commandE2AClass = commandTypeToCommandE2AClassMap.get(commandType);
    if (commandE2AClass == null) {
      throw new YExtMDecodeException("Can't find CommandE2A subclass for string command type: " + commandType);
    }
    try {
      return commandE2AClass.newInstance();
    } catch (InstantiationException | IllegalAccessException e) {
      throw new YExtMDecodeException("Can't instance " + commandE2AClass.getName() + "CommandE2A subclass for string command type: " + commandType, e);
    }
  }

  static {
    List<Class<? extends CommandE2A>> commandE2AClasses = new LinkedList<>();
    commandE2AClasses.add(MessageRequestCommandE2A.class);
    commandE2AClasses.add(MessageResponseCommandE2A.class);
    commandE2AClasses.add(InstallCommandE2A.class);
    commandE2AClasses.add(UninstallCommandE2A.class);
    commandE2AClasses.add(WatchCommandE2A.class);
    commandE2AClasses.add(UnwatchCommandE2A.class);
    commandE2AClasses.add(SetlocalCommandE2A.class);
    for (Class<? extends CommandE2A> commandE2AClass : commandE2AClasses) {
      CommandE2A commandE2A;
      try {
        commandE2A = commandE2AClass.newInstance();
      } catch (InstantiationException | IllegalAccessException e) {
        throw new AssertionError("Error initialization mapping of string command type to CommandE2A subclass");
      }
      commandTypeToCommandE2AClassMap.put(commandE2A.getCommandType(), commandE2AClass);
    }
  }

}
