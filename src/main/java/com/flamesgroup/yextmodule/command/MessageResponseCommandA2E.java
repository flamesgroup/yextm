package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

import java.util.Map;
import java.util.Map.Entry;

public class MessageResponseCommandA2E extends CommandA2E {

  private final String id;
  private final boolean processed;
  private final String name;
  private final String retvalue;
  private final Map<String, String> params;

  public MessageResponseCommandA2E(String id, boolean processed, String name, String retvalue, Map<String, String> params) {
    super();
    this.id = id;
    this.processed = processed;
    this.name = name;
    this.retvalue = retvalue;
    this.params = params;
  }

  @Override
  public String getCommandType() {
    return "<message";
  }

  public String getId() {
    return id;
  }

  public boolean isProcessed() {
    return processed;
  }

  public String getName() {
    return name;
  }

  public String getRetvalue() {
    return retvalue;
  }

  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    out.putString(id);
    out.putBoolean(processed);
    out.putString(name);
    out.putString(retvalue);
    for (Entry<String, String> entry : params.entrySet()) {
      out.putMapEntry(entry);
    }
  }

}
