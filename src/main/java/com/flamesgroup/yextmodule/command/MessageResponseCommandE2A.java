package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMDecodeException;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MessageResponseCommandE2A extends CommandE2A {

  private String id;
  private boolean processed;
  private String name;
  private String retvalue;
  private final Map<String, String> params = new HashMap<>();

  public MessageResponseCommandE2A() {
    super();
  }

  @Override
  public String getCommandType() {
    return "<message";
  }

  public String getId() {
    return id;
  }

  public boolean isProcessed() {
    return processed;
  }

  public String getName() {
    return name;
  }

  public String getRetvalue() {
    return retvalue;
  }

  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public void decodeArgs(CommandArgumentsDecode in) throws YExtMDecodeException {
    id = in.getString();
    processed = in.getBoolean();
    name = in.getString();
    retvalue = in.getString();
    while (in.hasMoreDataToDecode()) {
      Entry<String, String> entry = in.getMapEntry();
      params.put(entry.getKey(), entry.getValue());
    }
  }

}
