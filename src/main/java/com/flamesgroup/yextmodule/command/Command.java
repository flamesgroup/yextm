package com.flamesgroup.yextmodule.command;

public abstract class Command {

  public abstract String getCommandType();

}
