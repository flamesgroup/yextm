package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

public class UnwatchCommandA2E extends CommandA2E {

  private final String name;

  public UnwatchCommandA2E(String name) {
    this.name = name;
  }

  @Override
  public String getCommandType() {
    return ">unwatch";
  }

  public String getName() {
    return name;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    out.putString(name);
  }

}
