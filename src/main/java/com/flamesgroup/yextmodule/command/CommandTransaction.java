package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.IYExtMConnection;
import com.flamesgroup.yextmodule.YExtMException;
import com.flamesgroup.yextmodule.YExtMTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CommandTransaction<Request extends CommandA2E, Response extends CommandE2A> {

  private static enum State {
    IDLE, EXECUTING, EXECUTED
  }

  private final Logger logger = LoggerFactory.getLogger(CommandTransaction.class);

  private final IYExtMConnection connection;
  private final long responseTimeout;

  private final Lock responseLock = new ReentrantLock();
  private final Condition responseCondition = responseLock.newCondition();

  private State state = State.IDLE;
  private Response responseReceived;

  public CommandTransaction(IYExtMConnection connection, long responseTimeout) {
    super();
    this.connection = connection;
    this.responseTimeout = responseTimeout;
  }

  public Response execute(Request request) throws YExtMException, IOException, InterruptedException {
    responseLock.lock();
    try {
      if (state != State.IDLE) {
        throw new CommandTransactionNowExecutingException();
      }
      state = State.EXECUTING;
    } finally {
      responseLock.unlock();
    }

    try {
      connection.sendCommand(request);
      responseLock.lock();
      try {
        if (state == State.EXECUTING) {
          if (!responseCondition.await(responseTimeout, TimeUnit.MILLISECONDS)) {
            throw new YExtMTimeoutException("Response timeout elapsed for request [" + request + "]");
          }
        }
        if (state == State.EXECUTED) {
          return responseReceived;
        }
        throw new AssertionError();
      } finally {
        responseLock.unlock();
      }
    } finally {
      responseLock.lock();
      try {
        state = State.IDLE;
        responseReceived = null; // clear instance reference to Response object
      } finally {
        responseLock.unlock();
      }
    }
  }

  public void handleResponse(Response response) {
    responseLock.lock();
    try {
      if (state != State.EXECUTING) {
        logger.warn("Received unexpected response [{}]", response);
      } else {
        state = State.EXECUTED;
        responseReceived = response;
        responseCondition.signalAll();
      }
    } finally {
      responseLock.unlock();
    }
  }

}
