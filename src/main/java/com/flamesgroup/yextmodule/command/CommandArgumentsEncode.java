package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

import java.nio.BufferOverflowException;
import java.nio.CharBuffer;
import java.util.Map;

public final class CommandArgumentsEncode {

  private final CharBuffer argumentsCharBuffer;
  private boolean isFirstPut = true;

  public CommandArgumentsEncode(CharBuffer argumentsCharBuffer) {
    super();
    this.argumentsCharBuffer = argumentsCharBuffer;
  }

  private void putArgumentDelimeter() throws YExtMEncodeException {
    if (isFirstPut) {
      isFirstPut = false;
    } else {
      putCharWithoutEncode(':');
    }
  }

  public CommandArgumentsEncode putBoolean(boolean value) throws YExtMEncodeException {
    putArgumentDelimeter();
    putStringWithoutEncode(value ? "true" : "false");
    return this;
  }

  public CommandArgumentsEncode putLong(long value) throws YExtMEncodeException {
    putArgumentDelimeter();
    putStringWithoutEncode(Long.toString(value));
    return this;
  }

  public CommandArgumentsEncode putString(String value) throws YExtMEncodeException {
    return putString(value, true);
  }

  public CommandArgumentsEncode putString(String value, boolean escaped) throws YExtMEncodeException {
    putArgumentDelimeter();
    if (escaped) {
      putStringWithEncode(value);
    } else {
      putStringWithoutEncode(value);
    }
    return this;
  }

  public CommandArgumentsEncode putMapEntry(Map.Entry<String, String> entry) throws YExtMEncodeException {
    putArgumentDelimeter();
    putStringWithEncode(entry.getKey());
    putCharWithoutEncode('=');
    putStringWithEncode(entry.getValue());
    return this;
  }

  private void putCharWithoutEncode(char ch) throws YExtMEncodeException {
    try {
      argumentsCharBuffer.put(ch);
    } catch (BufferOverflowException e) {
      throw new YExtMEncodeException("Can't encode - arguments CharBuffer buffer is full", e);
    }
  }

  private void putStringWithoutEncode(String str) throws YExtMEncodeException {
    try {
      argumentsCharBuffer.put(str);
    } catch (BufferOverflowException e) {
      throw new YExtMEncodeException("Can't encode - arguments CharBuffer buffer is full", e);
    }
  }

  private void putStringWithEncode(String str) throws YExtMEncodeException {
    for (int i = 0; i < str.length(); i++) {
      char ch = str.charAt(i);
      if (ch < ' ' || ch == ':') {
        ch += '@';
        putCharWithoutEncode('%');
      } else if (ch == '%') {
        putCharWithoutEncode(ch);
      }
      putCharWithoutEncode(ch);
    }
  }

}
