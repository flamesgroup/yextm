package com.flamesgroup.yextmodule.command;

import com.flamesgroup.yextmodule.YExtMEncodeException;

public class SetlocalCommandA2E extends CommandA2E {

  private final String name;
  private final String value;

  public SetlocalCommandA2E(String name, String value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public String getCommandType() {
    return ">setlocal";
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }

  @Override
  public void encodeArgs(CommandArgumentsEncode out) throws YExtMEncodeException {
    out.putString(name);
    out.putString(value == null ? "" : value);
  }

}
