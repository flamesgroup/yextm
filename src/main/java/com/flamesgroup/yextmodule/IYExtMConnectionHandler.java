package com.flamesgroup.yextmodule;

import com.flamesgroup.yextmodule.command.CommandE2A;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CoderResult;

public interface IYExtMConnectionHandler {

  void handleReceiveCommand(CommandE2A commandE2A);

  void handleDisconnect();

  void handleException(IOException e);

  default void handleEncodeCharBufferError(final CharBuffer encodeCharBuffer, final CoderResult encodeCoderResult, final CoderResult encodeFlushCoderResult) {
  }

  default void handleDecodeByteBufferError(final ByteBuffer decodeByteBuffer, final CoderResult decodeCoderResult, final CoderResult decodeFlushCoderResult) {
  }

  default void handleCreateCommandException(final String commandType, final YExtMDecodeException e) {
  }

  default void handleDecodeCommandException(final String commandType, final YExtMDecodeException e) {
  }

}
