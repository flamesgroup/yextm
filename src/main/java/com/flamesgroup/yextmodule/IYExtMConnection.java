package com.flamesgroup.yextmodule;

import com.flamesgroup.yextmodule.command.CommandA2E;

import java.io.IOException;
import java.net.SocketAddress;

public interface IYExtMConnection {

  void connect(SocketAddress socketAddress, IYExtMConnectionHandler handler) throws IOException;

  void sendCommand(CommandA2E commandA2E) throws YExtMException, IOException;

  void disconnect() throws IOException;

  boolean isConnected();

}
