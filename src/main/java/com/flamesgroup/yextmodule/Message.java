package com.flamesgroup.yextmodule;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Message {
  private String id;
  private long time;
  private String name;
  private String retvalue;
  private Map<String, String> params;

  public Message(String id, long time, String name, String retvalue, Map<String, String> params) {
    if (id == null) {
      throw new IllegalArgumentException("id must not be null");
    }
    if (name == null) {
      throw new IllegalArgumentException("name must not be null");
    }
    if (retvalue == null) {
      throw new IllegalArgumentException("retvalue must not be null");
    }
    if (params == null) {
      throw new IllegalArgumentException("params must not be null");
    }
    this.id = id;
    this.time = time;
    this.name = name;
    this.retvalue = retvalue;
    this.params = params;
  }

  public Message(String name, Map<String, String> params) {
    this(UUID.randomUUID().toString(), TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), name, "", params);
  }

  public Message(String name) {
    this(UUID.randomUUID().toString(), TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), name, "", new HashMap<>());
  }

  public Message() {
    this(UUID.randomUUID().toString(), TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), "", "", new HashMap<>());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    if (id == null) {
      throw new IllegalArgumentException("id must not be null");
    }
    this.id = id;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    if (name == null) {
      throw new IllegalArgumentException("name must not be null");
    }
    this.name = name;
  }

  public String getRetvalue() {
    return retvalue;
  }

  public void setRetvalue(String retvalue) {
    if (retvalue == null) {
      throw new IllegalArgumentException("retvalue must not be null");
    }
    this.retvalue = retvalue;
  }

  public Map<String, String> getParams() {
    return params;
  }

  public void setParams(Map<String, String> params) {
    if (params == null) {
      throw new IllegalArgumentException("params must not be null");
    }
    this.params = params;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(64);
    sb.append(this.getClass().getName());
    sb.append('[');
    sb.append(name);
    sb.append(']');
    sb.append('\n');
    sb.append("  retvalue = ");
    sb.append(retvalue);
    sb.append('\n');
    for (Map.Entry<String, String> entry : params.entrySet()) {
      sb.append("  params['").append(entry.getKey()).append("'] = ").append(entry.getValue()).append('\n');
    }
    return sb.toString();
  }

}
