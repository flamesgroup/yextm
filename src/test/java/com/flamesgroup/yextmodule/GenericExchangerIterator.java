package com.flamesgroup.yextmodule;

import java.util.List;
import java.util.concurrent.Exchanger;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GenericExchangerIterator<SD, RD, SID extends GenericExchangerIterator.IterationData<SD>, RID extends GenericExchangerIterator.IterationData<RD>, I extends GenericExchangerIterator.Iteration<SID, RID>> {

  protected final List<I> iterations;
  private final Lock iterationNumberLock;
  private final Condition iterationNumberWaitConditions[];
  private int iterationNumber;

  private long iterationTimeout;

  private final Exchanger<SID> sendIterationDataExchanger = new Exchanger<>();
  private final Exchanger<Exception> sendIterationDataNotEqualsExceptionExchanger = new Exchanger<>();

  private FutureTask<Exception> iterationsFutureTask;

  public GenericExchangerIterator(final List<I> iterations) {
    this.iterations = iterations;
    iterationNumberLock = new ReentrantLock();
    iterationNumberWaitConditions = new Condition[iterations.size()];
    for (int n = 0; n < iterations.size(); n++) {
      iterationNumberWaitConditions[n] = iterationNumberLock.newCondition();
    }
    iterationTimeout = 0;
    for (I i : iterations) {
      if (iterationTimeout < i.getTimeout()) {
        iterationTimeout = i.getTimeout();
      }
    }
    iterationTimeout *= iterations.size();
    iterationTimeout *= 7; // magic factor
  }

  public void checkForException() throws Exception {
    Exception er = iterationsFutureTask.get();
    if (er != null) {
      throw er;
    }
  }

  public void waitForIterationNumber(final int n) throws Exception {
    iterationNumberLock.lock();
    try {
      if (n <= iterationNumber) {
        return;
      } else {
        if (iterationNumberWaitConditions[n].await(iterationTimeout, TimeUnit.MILLISECONDS)) {
          return;
        }
      }
    } finally {
      iterationNumberLock.unlock();
    }
    // condition timeout
    Exception e = iterationsFutureTask.get();
    throw new IllegalStateException(String.format("Iteration [%d] waiting timeout has expired", n), e);
  }

  protected void startIterate() {
    iterationsFutureTask = new FutureTask<>(() -> {
      iterationNumberLock.lock();
      try {
        iterationNumber = 0;
      } finally {
        iterationNumberLock.unlock();
      }
      for (final I iteration : iterations) {
        try {
          String handleThreadName;
          iterationNumberLock.lock();
          try {
            iterationNumberWaitConditions[iterationNumber].signalAll();
            handleThreadName = "handle Thread #" + iterationNumber;
          } finally {
            iterationNumberLock.unlock();
          }

          SID sendIterationData = iteration.getSendIterationData();
          if (sendIterationData != null) {
            sendIterationDataExchanger.exchange(sendIterationData);
            Exception sendIterationDataNotEqualsException = sendIterationDataNotEqualsExceptionExchanger.exchange(null);
            if (sendIterationDataNotEqualsException != null) {
              return sendIterationDataNotEqualsException;
            }
          }

          Thread.sleep(iteration.getTimeout());

          RID receivedIterationData = iteration.getReceivedIterationData();
          if (receivedIterationData != null) {
            FutureTask<Exception> handleFutureTask = createHandleReceiveFutureTask(receivedIterationData);
            new Thread(handleFutureTask, handleThreadName).start();

            Exception re = handleFutureTask.get();
            if (re != null) {
              return re;
            }
          }

          iterationNumberLock.lock();
          try {
            iterationNumber++;
          } finally {
            iterationNumberLock.unlock();
          }
        } catch (Exception e) {
          return e;
        }
      }
      return null;
    });

    new Thread(iterationsFutureTask, "iterations").start();
  }

  protected void send(final SID sendIterationData) throws InterruptedException {
    IllegalStateException sendIterationDataNotEqualsException = null;
    try {
      SID sendIterationDataExpected = sendIterationDataExchanger.exchange(null);
      boolean equal = sendIterationDataExpected.equals(sendIterationData);
      if (!equal) {
        sendIterationDataNotEqualsException = new IllegalStateException(String.format("Expected [%s] to send but send [%s]", sendIterationDataExpected.toString(), sendIterationData.toString()));
        throw sendIterationDataNotEqualsException;
      }
    } finally {
      sendIterationDataNotEqualsExceptionExchanger.exchange(sendIterationDataNotEqualsException);
    }
  }

  protected abstract FutureTask<Exception> createHandleReceiveFutureTask(final RID iterationData);

  public static class Iteration<SID extends GenericExchangerIterator.IterationData, RID extends GenericExchangerIterator.IterationData> {

    private final SID sendIterationData;
    private final long timeout;
    private final RID receivedIterationData;

    public Iteration(final SID sendIterationData, final long timeout, final RID receivedIterationData) {
      this.sendIterationData = sendIterationData;
      this.timeout = timeout;
      this.receivedIterationData = receivedIterationData;
    }

    public SID getSendIterationData() {
      return sendIterationData;
    }

    public long getTimeout() {
      return timeout;
    }

    public RID getReceivedIterationData() {
      return receivedIterationData;
    }

  }

  public static class IterationData<D> {

    private final D data;

    public IterationData(final D data) {
      this.data = data;
    }

    public D getData() {
      return data;
    }

    @Override
    public int hashCode() {
      return data.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
        return true;
      if (!(obj instanceof IterationData))
        return false;
      IterationData that = (IterationData) obj;
      return this.data.equals(that.data);
    }

  }

}
