package com.flamesgroup.yextmodule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.command.CommandE2A;
import com.flamesgroup.yextmodule.command.InstallCommandA2E;
import com.flamesgroup.yextmodule.command.InstallCommandE2A;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class YExtMConnectionInstallCommandTest {

  private final Semaphore semaphore = new Semaphore(0);

  private InstallCommandE2A installCommandE2A;

  @Test
  public void test() throws Exception {
    List<FakeYExtMChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeYExtMChannel.Iteration(
        "%%>install::test1\n".getBytes(),
        0,
        "%%<install:100:test1:true\n".getBytes()
    ));
    iterations.add(new FakeYExtMChannel.Iteration(
        "%%>install:1:test2\n".getBytes(),
        0,
        "%%<install:1:test2:false\n".getBytes()
    ));
    iterations.add(new FakeYExtMChannel.Iteration(
        "%%>install:10:test3:filterName1\n".getBytes(),
        0,
        "%%<install:10:test3:true\n".getBytes()
    ));
    iterations.add(new FakeYExtMChannel.Iteration(
        "%%>install::test4:filterName2:filterValue1\n".getBytes(),
        0,
        "%%<install:100:test4:false\n".getBytes()
    ));

    FakeYExtMChannel channel = new FakeYExtMChannel(iterations);
    IYExtMConnection connection = new YExtMConnection(channel);

    connection.connect(new InetSocketAddress("localhost", 10000), new IYExtMConnectionHandler() {
      @Override
      public void handleReceiveCommand(final CommandE2A commandE2A) {
        installCommandE2A = (InstallCommandE2A) commandE2A;
        semaphore.release();
      }

      @Override
      public void handleDisconnect() {
      }

      @Override
      public void handleException(final IOException e) {
      }
    });

    assertTrue(connection.isConnected());

    connection.sendCommand(new InstallCommandA2E("test1"));
    semaphore.acquire();
    assertEquals(100, installCommandE2A.getPriority());
    assertEquals("test1", installCommandE2A.getName());
    assertEquals(true, installCommandE2A.isSuccess());

    connection.sendCommand(new InstallCommandA2E(1, "test2"));
    semaphore.acquire();
    assertEquals(1, installCommandE2A.getPriority());
    assertEquals("test2", installCommandE2A.getName());
    assertEquals(false, installCommandE2A.isSuccess());

    connection.sendCommand(new InstallCommandA2E(10, "test3", "filterName1", null));
    semaphore.acquire();
    assertEquals(10, installCommandE2A.getPriority());
    assertEquals("test3", installCommandE2A.getName());
    assertEquals(true, installCommandE2A.isSuccess());

    connection.sendCommand(new InstallCommandA2E("test4", "filterName2", "filterValue1"));
    semaphore.acquire();
    assertEquals(100, installCommandE2A.getPriority());
    assertEquals("test4", installCommandE2A.getName());
    assertEquals(false, installCommandE2A.isSuccess());

    channel.checkForException();

    connection.disconnect();
    assertTrue(!connection.isConnected());
  }

}
