package com.flamesgroup.yextmodule;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.charset.CharacterCodingException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingDeque;

public class FakeYExtMChannel extends GenericByteBufferExchangerIterator<FakeYExtMChannel.IterationData, FakeYExtMChannel.Iteration> implements IYExtMChannel {

  private boolean opened = true;
  private boolean connected = false;

  private final BlockingQueue<ByteBuffer> byteBuffers = new LinkedBlockingDeque<>();

  public FakeYExtMChannel(final List<Iteration> iterations) {
    super(iterations);
  }

  @Override
  protected FutureTask<Exception> createHandleReceiveFutureTask(final IterationData iterationData) {
    return new FutureTask<>(() -> {
      try {
        byteBuffers.put(iterationData.getData());
        return null;
      } catch (Exception e) {
        e.printStackTrace();
        return e;
      }
    });
  }

  @Override
  public void connect(final SocketAddress socketAddress) throws IOException {
    connected = true;

    startIterate();
  }

  @Override
  public boolean isConnected() {
    return connected;
  }

  @Override
  public int read(final ByteBuffer dst) throws IOException {
    ByteBuffer src;
    try {
      src = byteBuffers.take();
    } catch (InterruptedException e) {
      throw new ClosedByInterruptException();
    }

    int remaining = src.remaining();
    dst.put(src);
    return remaining;
  }

  @Override
  public int write(final ByteBuffer src) throws IOException {
    int remaining = src.remaining();
    try {
      send(new IterationData(src));
    } catch (InterruptedException e) {
      throw new IOException(e);
    }
    return remaining;
  }

  @Override
  public boolean isOpen() {
    return opened;
  }

  @Override
  public void close() throws IOException {
    opened = false;
    connected = false;
  }

  public static class Iteration extends GenericByteBufferExchangerIterator.Iteration<FakeYExtMChannel.IterationData> {

    public Iteration(final ByteBuffer sendData, final long timeout, final ByteBuffer receivedData) throws CharacterCodingException {
      super(sendData == null ? null : new IterationData(sendData), timeout, receivedData == null ? null : new IterationData(receivedData));
    }

    public Iteration(final byte[] sendData, final long timeout, final byte[] receivedData) throws CharacterCodingException {
      super(sendData == null ? null : new IterationData(ByteBuffer.wrap(sendData)), timeout, receivedData == null ? null : new IterationData(ByteBuffer.wrap(receivedData)));
    }

  }

  public static class IterationData extends GenericByteBufferExchangerIterator.IterationData {

    public IterationData(final ByteBuffer data) {
      super(data);
    }

  }

}
