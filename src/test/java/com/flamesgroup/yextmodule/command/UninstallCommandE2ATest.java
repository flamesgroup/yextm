package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class UninstallCommandE2ATest extends CommandE2ATest {

  @Test
  public void uninstallCommandE2ACompareCommandType() throws YExtMEncodeException {
    UninstallCommandE2A uninstallCommandE2A = new UninstallCommandE2A();
    assertEquals("<uninstall", uninstallCommandE2A.getCommandType());
  }

  @Test
  public void uninstallCommandE2AConstructorAndDecodeArgs() throws YExtMDecodeException {
    UninstallCommandE2A uninstallCommandE2A = new UninstallCommandE2A();

    assertEquals(0, uninstallCommandE2A.getPriority());
    assertNull(uninstallCommandE2A.getName());
    assertFalse(uninstallCommandE2A.isSuccess());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("222:testName:false");
    argumentsCharBuffer.flip();
    uninstallCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals(222, uninstallCommandE2A.getPriority());
    assertEquals("testName", uninstallCommandE2A.getName());
    assertFalse(uninstallCommandE2A.isSuccess());
  }

}
