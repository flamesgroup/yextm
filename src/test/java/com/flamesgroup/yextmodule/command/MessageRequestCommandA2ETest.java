package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class MessageRequestCommandA2ETest extends CommandA2ETest {

  @Test
  public void messageRequestCommandA2ECompareCommandType() throws YExtMEncodeException {
    MessageRequestCommandA2E messageRequestCommandA2E = new MessageRequestCommandA2E("", 0, "", "", null);
    assertEquals(">message", messageRequestCommandA2E.getCommandType());
  }

  @Test(expected = NullPointerException.class)
  public void messageRequestCommandA2EConstructor0AndEncodeArgs() throws YExtMEncodeException {
    MessageRequestCommandA2E messageRequestCommandA2E = new MessageRequestCommandA2E("", 0, "", "", null);

    argumentsCharBuffer.clear();
    messageRequestCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();
  }

  @Test
  public void messageRequestCommandA2EConstructor1AndEncodeArgs() throws YExtMEncodeException {
    Map<String, String> params = new LinkedHashMap<>();
    MessageRequestCommandA2E messageRequestCommandA2E = new MessageRequestCommandA2E("id1", 100, "name1", "retvalue1", params);

    assertEquals("id1", messageRequestCommandA2E.getId());
    assertEquals(100, messageRequestCommandA2E.getTime());
    assertEquals("name1", messageRequestCommandA2E.getName());
    assertEquals("retvalue1", messageRequestCommandA2E.getRetvalue());
    assertEquals(params, messageRequestCommandA2E.getParams());

    argumentsCharBuffer.clear();
    messageRequestCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("id1:100:name1:retvalue1", argumentsCharBuffer.toString());
  }

  @Test
  public void messageRequestCommandA2EConstructor2AndEncodeArgs() throws YExtMEncodeException {
    Map<String, String> params = new LinkedHashMap<>();
    params.put("key1", "value1");
    params.put("", "");
    params.put("key3", "value3");
    MessageRequestCommandA2E messageRequestCommandA2E = new MessageRequestCommandA2E("id2", 100, "name2", "retvalue2", params);

    assertEquals("id2", messageRequestCommandA2E.getId());
    assertEquals(100, messageRequestCommandA2E.getTime());
    assertEquals("name2", messageRequestCommandA2E.getName());
    assertEquals("retvalue2", messageRequestCommandA2E.getRetvalue());
    assertEquals(params, messageRequestCommandA2E.getParams());

    argumentsCharBuffer.clear();
    messageRequestCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("id2:100:name2:retvalue2:key1=value1:=:key3=value3", argumentsCharBuffer.toString());
  }

}
