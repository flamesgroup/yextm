package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class UnwatchCommandE2ATest extends CommandE2ATest {

  @Test
  public void unwatchCommandE2ACompareCommandType() throws YExtMEncodeException {
    UnwatchCommandE2A unwatchCommandE2A = new UnwatchCommandE2A();
    assertEquals("<unwatch", unwatchCommandE2A.getCommandType());
  }

  @Test
  public void unwatchCommandE2AConstructorAndDecodeArgs() throws YExtMDecodeException {
    UnwatchCommandE2A unwatchCommandE2A = new UnwatchCommandE2A();

    assertNull(unwatchCommandE2A.getName());
    assertFalse(unwatchCommandE2A.isSuccess());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("testNameUnwatch:true");
    argumentsCharBuffer.flip();
    unwatchCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("testNameUnwatch", unwatchCommandE2A.getName());
    assertTrue(unwatchCommandE2A.isSuccess());
  }

}
