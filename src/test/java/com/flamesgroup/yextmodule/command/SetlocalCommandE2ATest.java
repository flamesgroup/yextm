package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class SetlocalCommandE2ATest extends CommandE2ATest {

  @Test
  public void setlocalCommandE2ACompareCommandType() throws YExtMEncodeException {
    SetlocalCommandE2A setlocalCommandE2A = new SetlocalCommandE2A();
    assertEquals("<setlocal", setlocalCommandE2A.getCommandType());
  }

  @Test
  public void setlocalCommandE2AConstructorAndDecodeArgs() throws YExtMDecodeException {
    SetlocalCommandE2A setlocalCommandE2A = new SetlocalCommandE2A();

    assertNull(setlocalCommandE2A.getName());
    assertNull(setlocalCommandE2A.getValue());
    assertFalse(setlocalCommandE2A.isSuccess());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("name:value:true");
    argumentsCharBuffer.flip();
    setlocalCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("name", setlocalCommandE2A.getName());
    assertEquals("value", setlocalCommandE2A.getValue());
    assertTrue(setlocalCommandE2A.isSuccess());
  }

}
