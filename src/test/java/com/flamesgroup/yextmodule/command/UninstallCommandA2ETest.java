package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class UninstallCommandA2ETest extends CommandA2ETest {

  @Test
  public void uninstallCommandA2ECompareCommandType() throws YExtMEncodeException {
    UninstallCommandA2E uninstallCommandA2E = new UninstallCommandA2E("");
    assertEquals(">uninstall", uninstallCommandA2E.getCommandType());
  }

  @Test
  public void uninstallCommandA2EConstructorAndEncodeArgs() throws YExtMEncodeException {
    UninstallCommandA2E uninstallCommandA2E = new UninstallCommandA2E("testName");

    assertEquals("testName", uninstallCommandA2E.getName());

    argumentsCharBuffer.clear();
    uninstallCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("testName", argumentsCharBuffer.toString());
  }

}
