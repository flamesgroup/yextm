package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class InstallCommandA2ETest extends CommandA2ETest {

  @Test
  public void installCommandA2ECompareCommandType() throws YExtMEncodeException {
    InstallCommandA2E installCommandA2E = new InstallCommandA2E(0, "", "", "");
    assertEquals(">install", installCommandA2E.getCommandType());
  }

  @Test
  public void installCommandA2EConstructor1AndEncodeArgs() throws YExtMEncodeException {
    InstallCommandA2E installCommandA2E = new InstallCommandA2E("testName1");

    assertEquals(100, installCommandA2E.getPriority());
    assertEquals("testName1", installCommandA2E.getName());
    assertNull(installCommandA2E.getFilterName());
    assertNull(installCommandA2E.getFilterValue());

    argumentsCharBuffer.clear();
    installCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals(":testName1", argumentsCharBuffer.toString());
  }

  @Test
  public void installCommandA2EConstructor2AndEncodeArgs() throws YExtMEncodeException {
    InstallCommandA2E installCommandA2E = new InstallCommandA2E(50, "testName2");

    assertEquals(50, installCommandA2E.getPriority());
    assertEquals("testName2", installCommandA2E.getName());
    assertNull(installCommandA2E.getFilterName());
    assertNull(installCommandA2E.getFilterValue());

    argumentsCharBuffer.clear();
    installCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("50:testName2", argumentsCharBuffer.toString());
  }

  @Test
  public void installCommandA2EConstructor3AndEncodeArgs() throws YExtMEncodeException {
    InstallCommandA2E installCommandA2E = new InstallCommandA2E("testName3", "filterName1", null);

    assertEquals(100, installCommandA2E.getPriority());
    assertEquals("testName3", installCommandA2E.getName());
    assertEquals("filterName1", installCommandA2E.getFilterName());
    assertNull(installCommandA2E.getFilterValue());

    argumentsCharBuffer.clear();
    installCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals(":testName3:filterName1", argumentsCharBuffer.toString());
  }

  @Test
  public void installCommandA2EConstructor4AndEncodeArgs() throws YExtMEncodeException {
    InstallCommandA2E installCommandA2E = new InstallCommandA2E("testName4", "filterName2", "filterValue1");

    assertEquals(100, installCommandA2E.getPriority());
    assertEquals("testName4", installCommandA2E.getName());
    assertEquals("filterName2", installCommandA2E.getFilterName());
    assertEquals("filterValue1", installCommandA2E.getFilterValue());

    argumentsCharBuffer.clear();
    installCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals(":testName4:filterName2:filterValue1", argumentsCharBuffer.toString());
  }

  @Test
  public void installCommandA2EConstructor5AndEncodeArgs() throws YExtMEncodeException {
    InstallCommandA2E installCommandA2E = new InstallCommandA2E(150, "testName5", "filterName3", "filterValue2");

    assertEquals(150, installCommandA2E.getPriority());
    assertEquals("testName5", installCommandA2E.getName());
    assertEquals("filterName3", installCommandA2E.getFilterName());
    assertEquals("filterValue2", installCommandA2E.getFilterValue());

    argumentsCharBuffer.clear();
    installCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("150:testName5:filterName3:filterValue2", argumentsCharBuffer.toString());
  }

}
