package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class WatchCommandE2ATest extends CommandE2ATest {

  @Test
  public void watchCommandE2ACompareCommandType() throws YExtMEncodeException {
    WatchCommandE2A watchCommandE2A = new WatchCommandE2A();
    assertEquals("<watch", watchCommandE2A.getCommandType());
  }

  @Test
  public void watchCommandE2AConstructorAndDecodeArgs() throws YExtMDecodeException {
    WatchCommandE2A watchCommandE2A = new WatchCommandE2A();

    assertNull(watchCommandE2A.getName());
    assertFalse(watchCommandE2A.isSuccess());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("testNameWatch:true");
    argumentsCharBuffer.flip();
    watchCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("testNameWatch", watchCommandE2A.getName());
    assertTrue(watchCommandE2A.isSuccess());
  }

}
