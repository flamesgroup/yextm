package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class OutputCommandA2ETest extends CommandA2ETest {

  @Test
  public void outputCommandA2ECompareCommandType() throws YExtMEncodeException {
    OutputCommandA2E outputCommandA2E = new OutputCommandA2E("");
    assertEquals(">output", outputCommandA2E.getCommandType());
  }

  @Test
  public void outputCommandA2EConstructorAndEncodeArgs() throws YExtMEncodeException {
    OutputCommandA2E outputCommandA2E = new OutputCommandA2E("output arbitrary unescaped string (% :) without newline delimeter");

    assertEquals("output arbitrary unescaped string (% :) without newline delimeter", outputCommandA2E.getOutput());

    argumentsCharBuffer.clear();
    outputCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("output arbitrary unescaped string (% :) without newline delimeter", argumentsCharBuffer.toString());
  }

}
