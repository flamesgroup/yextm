package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

import java.nio.CharBuffer;
import java.util.AbstractMap;

public class CommandArgumentsEncodeTest {

  private CharBuffer charBuffer;
  private CommandArgumentsEncode commandArgumentsEncode;

  @Test
  public void encodeCorrectData() throws YExtMEncodeException {
    charBuffer = CharBuffer.allocate(128);
    commandArgumentsEncode = new CommandArgumentsEncode(charBuffer);

    commandArgumentsEncode.putString("");
    commandArgumentsEncode.putBoolean(true);
    commandArgumentsEncode.putBoolean(false);
    commandArgumentsEncode.putLong(123);
    commandArgumentsEncode.putString("qwe");
    commandArgumentsEncode.putMapEntry(new AbstractMap.SimpleEntry<>("asd zxc", "q%1%z"));

    charBuffer.flip();
    assertEquals(":true:false:123:qwe:asd zxc=q%%1%%z", charBuffer.toString());
  }

  @Test
  public void encodeCorrectStringData() throws YExtMEncodeException {
    charBuffer = CharBuffer.allocate(128);
    commandArgumentsEncode = new CommandArgumentsEncode(charBuffer);

    commandArgumentsEncode.putString("(% :)");
    commandArgumentsEncode.putString("(% :)", false);
    commandArgumentsEncode.putString("(% :)", true);

    charBuffer.flip();
    assertEquals("(%% %z):(% :):(%% %z)", charBuffer.toString());
  }

  @Test(expected = YExtMEncodeException.class)
  public void encodeToEmptyCharBuffer() throws YExtMEncodeException {
    charBuffer = CharBuffer.allocate(0);
    commandArgumentsEncode = new CommandArgumentsEncode(charBuffer);

    commandArgumentsEncode.putString("empty");
  }

  @Test(expected = YExtMEncodeException.class)
  public void encodeToNotEnoughCharBuffer() throws YExtMEncodeException {
    charBuffer = CharBuffer.allocate(8);
    commandArgumentsEncode = new CommandArgumentsEncode(charBuffer);

    commandArgumentsEncode.putString("qwerty");
    commandArgumentsEncode.putLong(100000);
  }

}
