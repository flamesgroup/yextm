package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import org.junit.Test;

import java.nio.CharBuffer;
import java.util.AbstractMap;

public class CommandArgumentsDecodeTest {

  private CommandArgumentsDecode commandArgumentsDecode;

  public CommandArgumentsDecode createCommandArgumentsDecode(String str) {
    CharBuffer cb = CharBuffer.allocate(128);
    cb.append(str);
    cb.flip();
    return new CommandArgumentsDecode(cb);
  }

  public CommandArgumentsDecode createEmptyCommandArgumentsDecode() {
    return createCommandArgumentsDecode("");
  }

  @Test
  public void decodeCorrectData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode(":true:false:123:qwe:asd zxc=q%%1%%z");
    assertEquals("", commandArgumentsDecode.getString());
    assertEquals(true, commandArgumentsDecode.getBoolean());
    assertEquals(false, commandArgumentsDecode.getBoolean());
    assertEquals(123, commandArgumentsDecode.getLong());
    assertEquals("qwe", commandArgumentsDecode.getString());
    assertEquals(new AbstractMap.SimpleEntry<>("asd zxc", "q%1%z"), commandArgumentsDecode.getMapEntry());
  }

  @Test
  public void decodeCorrectBooleanData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("true");
    assertEquals(true, commandArgumentsDecode.getBoolean());
    commandArgumentsDecode = createCommandArgumentsDecode("false");
    assertEquals(false, commandArgumentsDecode.getBoolean());
  }

  @Test
  public void decodeCorrectLongData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("1");
    assertEquals(1, commandArgumentsDecode.getLong());
    commandArgumentsDecode = createCommandArgumentsDecode("-1");
    assertEquals(-1, commandArgumentsDecode.getLong());
  }

  @Test
  public void decodeCorrectStringData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("a");
    assertEquals("a", commandArgumentsDecode.getString());
    commandArgumentsDecode = createCommandArgumentsDecode("%%");
    assertEquals("%", commandArgumentsDecode.getString());
    commandArgumentsDecode = createCommandArgumentsDecode("%%");
    assertEquals("%%", commandArgumentsDecode.getString(false));
    commandArgumentsDecode = createCommandArgumentsDecode(new String(new char[] {'%', 5 + '@'}));
    assertEquals(String.valueOf((char) 5), commandArgumentsDecode.getString());
  }

  @Test
  public void decodeCorrectMapEntryData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("a=b");
    assertEquals(new AbstractMap.SimpleEntry<>("a", "b"), commandArgumentsDecode.getMapEntry());
    commandArgumentsDecode = createCommandArgumentsDecode("a=");
    assertEquals(new AbstractMap.SimpleEntry<>("a", ""), commandArgumentsDecode.getMapEntry());
    commandArgumentsDecode = createCommandArgumentsDecode("=b");
    assertEquals(new AbstractMap.SimpleEntry<>("", "b"), commandArgumentsDecode.getMapEntry());
  }

  @Test(expected = YExtMDecodeException.class)
  public void decodeIncorrectBooleanData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("qwerty");
    commandArgumentsDecode.getBoolean();
  }

  @Test(expected = YExtMDecodeException.class)
  public void decodeIncorrectLongData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("1q");
    commandArgumentsDecode.getLong();
  }

  @Test(expected = YExtMDecodeException.class)
  public void decodeIncorrectMapEntryData() throws YExtMDecodeException {
    commandArgumentsDecode = createCommandArgumentsDecode("qwerty");
    commandArgumentsDecode.getMapEntry();
  }

  @Test(expected = YExtMDecodeException.class)
  public void decodeLongEmptyData() throws YExtMDecodeException {
    commandArgumentsDecode = createEmptyCommandArgumentsDecode();
    commandArgumentsDecode.getLong();
  }

  @Test(expected = YExtMDecodeException.class)
  public void decodeMapEntryEmptyData() throws YExtMDecodeException {
    commandArgumentsDecode = createEmptyCommandArgumentsDecode();
    commandArgumentsDecode.getMapEntry();
  }

}
