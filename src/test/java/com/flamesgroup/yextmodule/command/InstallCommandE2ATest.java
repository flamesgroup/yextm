package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class InstallCommandE2ATest extends CommandE2ATest {

  @Test
  public void installCommandE2ACompareCommandType() throws YExtMEncodeException {
    InstallCommandE2A installCommandE2A = new InstallCommandE2A();
    assertEquals("<install", installCommandE2A.getCommandType());
  }

  @Test
  public void installCommandE2AConstructorAndDecodeArgs() throws YExtMDecodeException {
    InstallCommandE2A installCommandE2A = new InstallCommandE2A();

    assertEquals(0, installCommandE2A.getPriority());
    assertNull(installCommandE2A.getName());
    assertFalse(installCommandE2A.isSuccess());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("111:testName:true");
    argumentsCharBuffer.flip();
    installCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals(111, installCommandE2A.getPriority());
    assertEquals("testName", installCommandE2A.getName());
    assertTrue(installCommandE2A.isSuccess());
  }

}
