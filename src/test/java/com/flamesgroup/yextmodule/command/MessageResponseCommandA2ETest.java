package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class MessageResponseCommandA2ETest extends CommandA2ETest {

  @Test
  public void messageResponseCommandA2ECompareCommandType() throws YExtMEncodeException {
    MessageResponseCommandA2E messageResponseCommandA2E = new MessageResponseCommandA2E("", false, "", "", null);
    assertEquals("<message", messageResponseCommandA2E.getCommandType());
  }

  @Test(expected = NullPointerException.class)
  public void messageResponseCommandA2EConstructor0AndEncodeArgs() throws YExtMEncodeException {
    MessageResponseCommandA2E messageResponseCommandA2E = new MessageResponseCommandA2E("", false, "", "", null);

    argumentsCharBuffer.clear();
    messageResponseCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();
  }

  @Test
  public void messageResponseCommandA2EConstructor1AndEncodeArgs() throws YExtMEncodeException {
    Map<String, String> params = new LinkedHashMap<>();
    MessageResponseCommandA2E messageResponseCommandA2E = new MessageResponseCommandA2E("id1", true, "name1", "retvalue1", params);

    assertEquals("id1", messageResponseCommandA2E.getId());
    assertTrue(messageResponseCommandA2E.isProcessed());
    assertEquals("name1", messageResponseCommandA2E.getName());
    assertEquals("retvalue1", messageResponseCommandA2E.getRetvalue());
    assertEquals(params, messageResponseCommandA2E.getParams());

    argumentsCharBuffer.clear();
    messageResponseCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("id1:true:name1:retvalue1", argumentsCharBuffer.toString());
  }

  @Test
  public void messageResponseCommandA2EConstructor2AndEncodeArgs() throws YExtMEncodeException {
    Map<String, String> params = new LinkedHashMap<>();
    params.put("key1", "value1");
    params.put("", "");
    params.put("key3", "value3");
    MessageResponseCommandA2E messageResponseCommandA2E = new MessageResponseCommandA2E("id2", true, "name2", "retvalue2", params);

    assertEquals("id2", messageResponseCommandA2E.getId());
    assertTrue(messageResponseCommandA2E.isProcessed());
    assertEquals("name2", messageResponseCommandA2E.getName());
    assertEquals("retvalue2", messageResponseCommandA2E.getRetvalue());
    assertEquals(params, messageResponseCommandA2E.getParams());

    argumentsCharBuffer.clear();
    messageResponseCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("id2:true:name2:retvalue2:key1=value1:=:key3=value3", argumentsCharBuffer.toString());
  }

}
