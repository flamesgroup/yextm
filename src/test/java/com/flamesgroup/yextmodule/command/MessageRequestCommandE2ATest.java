package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class MessageRequestCommandE2ATest extends CommandE2ATest {

  @Test
  public void messageRequestCommandE2ACompareCommandType() throws YExtMEncodeException {
    MessageRequestCommandE2A messageRequestCommandE2A = new MessageRequestCommandE2A();
    assertEquals(">message", messageRequestCommandE2A.getCommandType());
  }

  @Test
  public void messageRequestCommandE2AConstructor1AndEncodeArgs() throws YExtMDecodeException {
    MessageRequestCommandE2A messageRequestCommandE2A = new MessageRequestCommandE2A();

    assertNull(messageRequestCommandE2A.getId());
    assertEquals(0, messageRequestCommandE2A.getTime());
    assertNull(messageRequestCommandE2A.getName());
    assertNull(messageRequestCommandE2A.getRetvalue());
    assertNotNull(messageRequestCommandE2A.getParams());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("id1:100:name1:retvalue1");
    argumentsCharBuffer.flip();
    messageRequestCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("id1", messageRequestCommandE2A.getId());
    assertEquals(100, messageRequestCommandE2A.getTime());
    assertEquals("name1", messageRequestCommandE2A.getName());
    assertEquals("retvalue1", messageRequestCommandE2A.getRetvalue());
    assertTrue(messageRequestCommandE2A.getParams().isEmpty());
  }

  @Test
  public void messageRequestCommandE2AConstructor2AndEncodeArgs() throws YExtMDecodeException {
    MessageRequestCommandE2A messageRequestCommandE2A = new MessageRequestCommandE2A();

    assertNull(messageRequestCommandE2A.getId());
    assertEquals(0, messageRequestCommandE2A.getTime());
    assertNull(messageRequestCommandE2A.getName());
    assertNull(messageRequestCommandE2A.getRetvalue());
    assertNotNull(messageRequestCommandE2A.getParams());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("id2:100:name2:retvalue2:key1=value1:=:key3=value3");
    argumentsCharBuffer.flip();
    messageRequestCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("id2", messageRequestCommandE2A.getId());
    assertEquals(100, messageRequestCommandE2A.getTime());
    assertEquals("name2", messageRequestCommandE2A.getName());
    assertEquals("retvalue2", messageRequestCommandE2A.getRetvalue());
    Map<String, String> params = new LinkedHashMap<>();
    params.put("key1", "value1");
    params.put("", "");
    params.put("key3", "value3");
    assertTrue(messageRequestCommandE2A.getParams().equals(params));
  }

}
