package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class UnwatchCommandA2ETest extends CommandA2ETest {

  @Test
  public void unwatchCommandA2ECompareCommandType() throws YExtMEncodeException {
    UnwatchCommandA2E unwatchCommandA2E = new UnwatchCommandA2E("");
    assertEquals(">unwatch", unwatchCommandA2E.getCommandType());
  }

  @Test
  public void unwatchCommandA2EConstructorAndEncodeArgs() throws YExtMEncodeException {
    UnwatchCommandA2E unwatchCommandA2E = new UnwatchCommandA2E("testNameUnwatch");

    assertEquals("testNameUnwatch", unwatchCommandA2E.getName());

    argumentsCharBuffer.clear();
    unwatchCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("testNameUnwatch", argumentsCharBuffer.toString());
  }

}
