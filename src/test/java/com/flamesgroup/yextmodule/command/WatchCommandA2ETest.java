package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class WatchCommandA2ETest extends CommandA2ETest {

  @Test
  public void watchCommandA2ECompareCommandType() throws YExtMEncodeException {
    WatchCommandA2E watchCommandA2E = new WatchCommandA2E("testNameWatch");
    assertEquals(">watch", watchCommandA2E.getCommandType());
  }

  @Test
  public void watchCommandA2EConstructorAndEncodeArgs() throws YExtMEncodeException {
    WatchCommandA2E watchCommandA2E = new WatchCommandA2E("testNameWatch");

    assertEquals("testNameWatch", watchCommandA2E.getName());

    argumentsCharBuffer.clear();
    watchCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("testNameWatch", argumentsCharBuffer.toString());
  }

}
