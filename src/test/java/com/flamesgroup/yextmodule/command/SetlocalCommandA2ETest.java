package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

public class SetlocalCommandA2ETest extends CommandA2ETest {

  @Test
  public void setlocalCommandA2ECompareCommandType() throws YExtMEncodeException {
    SetlocalCommandA2E setlocalCommandA2E = new SetlocalCommandA2E("", "");
    assertEquals(">setlocal", setlocalCommandA2E.getCommandType());
  }

  @Test
  public void setlocalCommandA2EConstructor1AndEncodeArgs() throws YExtMEncodeException {
    SetlocalCommandA2E setlocalCommandA2E = new SetlocalCommandA2E("name", "value");

    assertEquals("name", setlocalCommandA2E.getName());
    assertEquals("value", setlocalCommandA2E.getValue());

    argumentsCharBuffer.clear();
    setlocalCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("name:value", argumentsCharBuffer.toString());
  }

  @Test
  public void setlocalCommandA2EConstructor2AndEncodeArgs() throws YExtMEncodeException {
    SetlocalCommandA2E setlocalCommandA2E = new SetlocalCommandA2E("name", null);

    assertEquals("name", setlocalCommandA2E.getName());
    assertEquals(null, setlocalCommandA2E.getValue());

    argumentsCharBuffer.clear();
    setlocalCommandA2E.encodeArgs(new CommandArgumentsEncode(argumentsCharBuffer));
    argumentsCharBuffer.flip();

    assertEquals("name:", argumentsCharBuffer.toString());
  }

}
