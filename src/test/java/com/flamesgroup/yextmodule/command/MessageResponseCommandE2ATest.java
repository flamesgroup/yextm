package com.flamesgroup.yextmodule.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.YExtMDecodeException;
import com.flamesgroup.yextmodule.YExtMEncodeException;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class MessageResponseCommandE2ATest extends CommandE2ATest {

  @Test
  public void messageResponseCommandE2ACompareCommandType() throws YExtMEncodeException {
    MessageResponseCommandE2A messageResponseCommandE2A = new MessageResponseCommandE2A();
    assertEquals("<message", messageResponseCommandE2A.getCommandType());
  }

  @Test
  public void messageResponseCommandE2AConstructor1AndEncodeArgs() throws YExtMDecodeException {
    MessageResponseCommandE2A messageResponseCommandE2A = new MessageResponseCommandE2A();

    assertNull(messageResponseCommandE2A.getId());
    assertFalse(messageResponseCommandE2A.isProcessed());
    assertNull(messageResponseCommandE2A.getName());
    assertNull(messageResponseCommandE2A.getRetvalue());
    assertNotNull(messageResponseCommandE2A.getParams());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("id1:true:name1:retvalue1");
    argumentsCharBuffer.flip();
    messageResponseCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("id1", messageResponseCommandE2A.getId());
    assertTrue(messageResponseCommandE2A.isProcessed());
    assertEquals("name1", messageResponseCommandE2A.getName());
    assertEquals("retvalue1", messageResponseCommandE2A.getRetvalue());
    assertTrue(messageResponseCommandE2A.getParams().isEmpty());
  }

  @Test
  public void messageResponseCommandE2AConstructor2AndEncodeArgs() throws YExtMDecodeException {
    MessageResponseCommandE2A messageResponseCommandE2A = new MessageResponseCommandE2A();

    assertNull(messageResponseCommandE2A.getId());
    assertFalse(messageResponseCommandE2A.isProcessed());
    assertNull(messageResponseCommandE2A.getName());
    assertNull(messageResponseCommandE2A.getRetvalue());
    assertNotNull(messageResponseCommandE2A.getParams());

    argumentsCharBuffer.clear();
    argumentsCharBuffer.put("id2:true:name2:retvalue2:key1=value1:=:key3=value3");
    argumentsCharBuffer.flip();
    messageResponseCommandE2A.decodeArgs(new CommandArgumentsDecode(argumentsCharBuffer));

    assertEquals("id2", messageResponseCommandE2A.getId());
    assertTrue(messageResponseCommandE2A.isProcessed());
    assertEquals("name2", messageResponseCommandE2A.getName());
    assertEquals("retvalue2", messageResponseCommandE2A.getRetvalue());
    Map<String, String> params = new LinkedHashMap<>();
    params.put("key1", "value1");
    params.put("", "");
    params.put("key3", "value3");
    assertTrue(messageResponseCommandE2A.getParams().equals(params));
  }

}
