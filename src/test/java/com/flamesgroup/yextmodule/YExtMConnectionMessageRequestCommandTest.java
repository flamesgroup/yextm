package com.flamesgroup.yextmodule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.yextmodule.command.CommandE2A;
import com.flamesgroup.yextmodule.command.MessageRequestCommandE2A;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.CoderResult;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class YExtMConnectionMessageRequestCommandTest {

  @Test
  public void testNonAsciiCharsetParameter() throws Exception {
    List<FakeYExtMChannel.Iteration> iterations = new ArrayList<>();
    iterations.add(new FakeYExtMChannel.Iteration(null, 0, "%%>message:1:1480689875:test::id=1:name=Тест:module=test\n".getBytes()));

    iterations.add(new FakeYExtMChannel.Iteration(null, 0, "%%>message:2:1480689875:test::id=2:name=Test:module=test\n".getBytes()));

    FakeYExtMChannel channel = new FakeYExtMChannel(iterations);
    IYExtMConnection connection = new YExtMConnection(channel);

    CountDownLatch countDownLatch = new CountDownLatch(2);
    List<MessageRequestCommandE2A> messageRequestCommandE2AList = new ArrayList<>();
    connection.connect(new InetSocketAddress("localhost", 10000), new IYExtMConnectionHandler() {
      @Override
      public void handleReceiveCommand(final CommandE2A commandE2A) {
        messageRequestCommandE2AList.add((MessageRequestCommandE2A) commandE2A);
        countDownLatch.countDown();
      }

      @Override
      public void handleDisconnect() {
      }

      @Override
      public void handleException(final IOException e) {
      }

    });

    assertTrue(connection.isConnected());

    countDownLatch.await();

    assertEquals(2, messageRequestCommandE2AList.size());

    MessageRequestCommandE2A messageRequestCommandE2A = messageRequestCommandE2AList.get(0);
    assertEquals("1", messageRequestCommandE2A.getId());
    assertEquals("test", messageRequestCommandE2A.getName());
    assertEquals(3, messageRequestCommandE2A.getParams().size());
    assertEquals("1", messageRequestCommandE2A.getParams().get("id"));
    assertEquals("Тест", messageRequestCommandE2A.getParams().get("name"));
    assertEquals("test", messageRequestCommandE2A.getParams().get("module"));

    messageRequestCommandE2A = messageRequestCommandE2AList.get(1);
    assertEquals("2", messageRequestCommandE2A.getId());
    assertEquals("test", messageRequestCommandE2A.getName());
    assertEquals(3, messageRequestCommandE2A.getParams().size());
    assertEquals("2", messageRequestCommandE2A.getParams().get("id"));
    assertEquals("Test", messageRequestCommandE2A.getParams().get("name"));
    assertEquals("test", messageRequestCommandE2A.getParams().get("module"));

    channel.checkForException();

    connection.disconnect();
    assertTrue(!connection.isConnected());
  }

  @Test
  public void testMalformedCharsetParameter() throws Exception {
    List<FakeYExtMChannel.Iteration> iterations = new ArrayList<>();

    iterations.add(new FakeYExtMChannel.Iteration(null, 0,
        new byte[] {0x25, 0x25, 0x3E, 0x6D, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x3A, 0x31, 0x3A, 0x31, 0x34, 0x38, 0x30, 0x36, 0x38, 0x39, 0x38, 0x37, 0x35, 0x3A, 0x74, 0x65, 0x73, 0x74, 0x3A, 0x3A,
            0x69, 0x64, 0x3D, 0x31, 0x3A, 0x6E, 0x61, 0x6D, 0x65, 0x3D, (byte) 0x80, 0x65, 0x73, 0x74, 0x3A, 0x6D, 0x6F, 0x64, 0x75, 0x6C, 0x65, 0x3D, 0x74, 0x65, 0x73, 0x74, 0x0A}));

    iterations.add(new FakeYExtMChannel.Iteration(null, 0, "%%>message:2:1480689875:test::id=2:name=Test:module=test\n".getBytes()));

    FakeYExtMChannel channel = new FakeYExtMChannel(iterations);
    IYExtMConnection connection = new YExtMConnection(channel);

    AtomicReference<CoderResult> decodeCoderResult = new AtomicReference<>();
    AtomicReference<CoderResult> decodeFlushCoderResult = new AtomicReference<>();
    AtomicReference<String> commandType = new AtomicReference<>();
    AtomicReference<YExtMDecodeException> decodeException = new AtomicReference<>();

    CountDownLatch countDownLatch = new CountDownLatch(2);
    connection.connect(new InetSocketAddress("localhost", 10000), new IYExtMConnectionHandler() {
      @Override
      public void handleReceiveCommand(final CommandE2A commandE2A) {
      }

      @Override
      public void handleDisconnect() {
      }

      @Override
      public void handleException(final IOException e) {
      }

      @Override
      public void handleDecodeByteBufferError(final ByteBuffer decodeByteBufferLocal, final CoderResult decodeCoderResultLocal, final CoderResult decodeFlushCoderResultLocal) {
        decodeCoderResult.set(decodeCoderResultLocal);
        decodeFlushCoderResult.set(decodeFlushCoderResultLocal);
        countDownLatch.countDown();
      }

      @Override
      public void handleDecodeCommandException(final String commandTypeLocal, final YExtMDecodeException e) {
        commandType.set(commandTypeLocal);
        decodeException.set(e);
        countDownLatch.countDown();
      }
    });

    assertTrue(connection.isConnected());

    countDownLatch.await();

    assertTrue(decodeCoderResult.get().isError());
    assertFalse(decodeFlushCoderResult.get().isError());

    assertEquals(">message", commandType.get());
    assertEquals("Can't decode MapEntry (equal character isn't exist) from string [2]", decodeException.get().getMessage());

    channel.checkForException();

    connection.disconnect();
    assertTrue(!connection.isConnected());
  }

}
