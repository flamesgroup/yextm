package com.flamesgroup.yextmodule;

import java.nio.ByteBuffer;
import java.util.List;

public abstract class GenericByteBufferExchangerIterator<ID extends GenericByteBufferExchangerIterator.IterationData, I extends GenericByteBufferExchangerIterator.Iteration<ID>>
    extends GenericExchangerIterator<ByteBuffer, ByteBuffer, ID, ID, I> {

  protected GenericByteBufferExchangerIterator(final List<I> iterations) {
    super(iterations);
  }

  public static class Iteration<ID extends IterationData> extends GenericExchangerIterator.Iteration<ID, ID> {

    public Iteration(final ID sendIterationData, final long timeout, final ID receivedIterationData) {
      super(sendIterationData, timeout, receivedIterationData);
    }

  }

  public static class IterationData extends GenericExchangerIterator.IterationData<ByteBuffer> {

    public IterationData(final ByteBuffer data) {
      super(data);
    }

  }
}
